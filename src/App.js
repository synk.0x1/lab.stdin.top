import React from 'react';
import './App.scss';

// Main Routes
import Routes from './components/Routes'

//core
import Header from './components/core/Header'
import Footer from './components/core/Footer'


function App() {
  return (
    <div>
    <Header />
    <div className="main-container">
      <Routes /> 
    </div>
    <Footer />  
    </div>
  );
}

export default App;
