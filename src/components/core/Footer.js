import React from 'react'
import { FontAwesomeIcon } from '../../../node_modules/@fortawesome/react-fontawesome'
import { faHeart } from '../../../node_modules/@fortawesome/free-solid-svg-icons'
import { faReact } from '../../../node_modules/@fortawesome/free-brands-svg-icons'
class Footer extends React.Component{
    render() {
        return <div>

<footer><div className="footer">
  <p>Made with <FontAwesomeIcon icon={faHeart} /> & <FontAwesomeIcon icon={faReact} />   &copy; Copyright 2020, <a href="https://stdin.top/">Jagadeesh Kotra</a></p>
</div></footer>
        </div>
    }
}

export default Footer;
