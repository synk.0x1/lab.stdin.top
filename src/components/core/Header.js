import React from 'react'
import {Navbar, Nav, NavDropdown, Button} from 'react-bootstrap';
import experiment_logo from '../static/experiment.svg'

import { FontAwesomeIcon } from '../../../node_modules/@fortawesome/react-fontawesome'
import { faTwitter } from '../../../node_modules/@fortawesome/free-brands-svg-icons'
import { faEnvelope } from '../../../node_modules/@fortawesome/free-regular-svg-icons'

class Header extends React.Component {
    render() {
        return <div>
        <Navbar collapseOnSelect bg="light" expand="lg">
          <Navbar.Brand href="/#">
          <img
        alt=""
        src={experiment_logo}
        width="30"
        height="30"
        className="d-inline-block align-top"
      />
            
          Laboratory</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/#">Home</Nav.Link>
              <Nav.Link href="#/about/">About</Nav.Link>
              <NavDropdown title="Projects" id="basic-nav-dropdown">
                <NavDropdown.Item href="#/myip/">My IP</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
              </NavDropdown>
            </Nav>

              <Button className="email" href="mailto:jagadeesh@stdin.top"><FontAwesomeIcon icon={faEnvelope} /></Button>
              <Button href="https://twitter.com/jagadeesh_kotra"><FontAwesomeIcon icon={faTwitter} /></Button>

          </Navbar.Collapse>
        </Navbar>
                </div>
    }
}

export default Header;