import React from 'react';
import {HashRouter as Router, Route, Switch} from 'react-router-dom'
import Homepage from './pages/Homepage'
import About from './pages/About'
import MyIp from './pages/MyIp'


class Routes extends React.Component {

    render(){

        return <Router basename={process.env.PUBLIC_URL}>

            <Switch>
                <Route path="/" component={Homepage} exact />
                <Route path="/about" component={About} />
                <Route path="/myip" component={MyIp} />
            </Switch>

        </Router>

    }

}

export default Routes;
