import React from 'react';

//extras
import WelcomeMsg from '../core/extras/WelcomeMsg.js'

class Homepage extends React.Component {

    render(){
        return <div>
        <h1> Welcome to lab! </h1>
        <WelcomeMsg />
        </div>
    }

}

export default Homepage;
