import React from 'react';
const publicIp = require('public-ip');


class MyIp extends React.Component {
    constructor(props){
        super(props)
        this.state = {'ipv4': '0.0.0.0', 'ok': false}
    }

    async get_ip_user_ipv4(){
        let ip = await publicIp.v4();
        return ip
    }


    async set_ip(){
        
        var curr_ip_ipv4;

        try{

            curr_ip_ipv4 = await this.get_ip_user_ipv4();
            
        }catch(err){ console.log(err); curr_ip_ipv4=undefined; }

        if (!(curr_ip_ipv4 === undefined)){
        this.setState({'ipv4': curr_ip_ipv4})
        this.setState({'ok': true})
        }

        console.log(this.state.ipv4);
    }

    render(){

        return <div>
            
            {this.state.ok ? 
            <div className="alert alert-primary" role="alert">
                <h3>Your IP: {this.state.ipv4}</h3>
                </div>: null}
            <button className="btn btn-success" onClick={() => this.set_ip()}>Get IP!</button>
        </div>

    }

}

export default MyIp;
